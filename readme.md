betterrev
=========

A friendly, hosted wrapper around webrevs

Getting started for developers
------------------------------

* [Download and install play](http://www.playframework.com/documentation/2.1.1/Installing)
* Clone the [betterrev repository](https://bitbucket.org/adoptopenjdk/betterrev.git):
    `$ git clone https://bitbucket.org/adoptopenjdk/betterrev.git`
* Clone the [adopt repository](https://bitbucket.org/adoptopenjdk/adopt) into the betterrev repository:
    `$ cd betterrev/betterrev && hg clone https://bitbucket.org/adoptopenjdk/adopt`
* Go to the betterrev directory and type `play run`
* Launch [Betterrev](http://localhost:9000/)


Developing using IntelliJ IDEA 12
---------------------------------

The following is a quick start guide on how to develop with and compile the betterrev project using IntelliJ IDEA 12.
I've tested these instructions using the Ultimate Edition, and I believe they should work on the Community Version,
but a different version of the Scala plugin may be required (if anyone attempts this could they please update the wiki :- )

- Download and install the latest version of the [IDEA Scala Plugin](http://plugins.jetbrains.com/plugin/?idea&id=1347 "IDEA Scala Plugin")

- Download install the the latest version of the [IDEA Play 2 Plugin](http://plugins.jetbrains.com/plugin?pr=idea&pluginId=7080 "IDEA Play 2 Plugin")

- Open a Command Line/Terminal and cd to the betterrev directory in the root of the project e.g. <betterrev_project_root>/betterrev/
- Automagically create IDEA project files (this removes the need for you to create your own IDEA project and import the original sources)
```
$ play idea
```
- Compile the project (to prevent compile warnings from being displayed in IDEA)
```
$ play compile
```
It's worth noting at this point that you should ensure that JDK path on the command line is the same as the IDEA JDK. I received the classic
'Generic Major/Minor warning' message when attempting to start the app via IDEA as I had compiled the app using the 1.8-EA JDK on the CLI path, and the
default JDK in IDEA was set to 1.7_09

- Start IDEA and open the project via File -> Open
- Start coding! :-)

Developing using Eclipse Juno
-----------------------------

The following is a quick start guide on how to develop with and compile the betterrev project using Eclipse Juno.

Download and install the latest version of the Eclipse Scala IDE Plugin from the Eclipse marketplace

Open a Command Line/Terminal and cd to the betterrev directory in the root of the project e.g. <betterrev_project_root>/betterrev/
Automagically create Eclipse project files (this removes the need for you to create your own Eclipse project and import the original sources)
```
$ play eclipse
```
Compile the project (to prevent compile warnings from being displayed in Eclipse)
```
$ play compile
```
Start Eclipse and import the project via File -> Import --> General --> Existing Projects into Workspace

Afterwards you'll still have compiler errors, change to using a JDK 1.7.0_10+ JVM and set the source level to 1.7

- Start coding! :-)
