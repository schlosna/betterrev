package models;

import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Basic User persistence tests.
 * TODO: Tidy tests so they have one assert each
 */
public class UserTest extends AbstractPersistenceIntegrationTest {

    private static final String TEST_BITBUCKET_USERNAME = "danielbryant_uk";
    private static final String TEST_NAME = "Daniel Bryant";
    private static final User.OcaStatus TEST_OCA_SIGNED = User.OcaStatus.SIGNED;

    public static User createTestInstance() {
        return new User(TEST_BITBUCKET_USERNAME, TEST_NAME, TEST_OCA_SIGNED);
    }

    @Test
    public void save_validUser_userPersisted() {
        User user = createTestInstance();
        assertThat(user.id, is(nullValue()));

        user.save();

        assertThat(user.id, is(not(nullValue())));
        assertThat(user.createdDate, is(notNullValue()));
    }
}
