package models;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import play.libs.Yaml;

import com.avaje.ebean.Ebean;

/**
 * Basic Mentor entity persistence tests.
 * TODO: Tidy tests so they have one assert each
 */
public class MentorTest extends AbstractPersistenceIntegrationTest {

    private static final String TEST_MENTOR_NAME = "Test Mentor";
    private static final String TEST_MENTOR_EMAIL = "mentor@java.net";
    private static final Mentor.MentorType TEST_MENTOR_TYPE = Mentor.MentorType.INDIVIDUAL;
    private static final String MENTORS_FILE = "mentors.yml";;

    public static Mentor createTestInstance() {
        Mentor mentor = new Mentor(TEST_MENTOR_NAME, TEST_MENTOR_EMAIL, TEST_MENTOR_TYPE);
        mentor.interests.add(InterestTest.createTestInstance());
        return mentor;
    }

    @Test
    public void save_validMentor_mentorPersisted() {
        Mentor mentor = createTestInstance();
        assertThat(mentor.id, is(nullValue()));
        mentor.save();
        assertThat(mentor.id, is(not(nullValue())));
    }
    
    public void loadMentors() {
        String dropScript = ddl.generateDropDdl();
        ddl.runScript(false, dropScript);

        String createScript = ddl.generateCreateDdl();
        ddl.runScript(false, createScript);

        @SuppressWarnings("unchecked")
        Map<String, List<Object>> all = (Map<String, List<Object>>) Yaml.load(MENTORS_FILE);
        Ebean.save(all.get("mentors"));
    }

    @Test
    public void findsRelevantMentors() {
        loadMentors();
        List<Mentor> mentors = Mentor.findRelevantMentors("hotspot", "./test/compiler/6775880/Test.java");
        assertEquals(1, mentors.size());
        
        Mentor mentor = mentors.get(0);
        assertEquals("Hotspot", mentor.name);
        assertEquals("hotspot-dev@openjdk.java.net", mentor.email);
    }

}
