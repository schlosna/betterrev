package models;

import com.avaje.ebean.Ebean;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import play.libs.Yaml;

import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Basic Tag persistence tests.
 * TODO: Tidy tests so they have one assert each
 */
public class TagTest extends AbstractPersistenceIntegrationTest {

    public static final String TEST_TAG_NAME = "Test Tag";
    public static final String TEST_TAG_DESCRIPTION = "Test Tag Description";

    public static final Long INIT_DATA_TEST_TAG_ID = Long.valueOf(999);
    public static final String INIT_DATA_TEST_TAG_NAME = "Hackday One";

    public static final Long INIT_DATA_TEST_TAG_ID_DOESNT_EXIST = Long.valueOf(777777);

    public static Tag createTestInstance() {
        return new Tag(TEST_TAG_NAME, TEST_TAG_DESCRIPTION);
    }

    @Before
    /**
     * Drops, creates and then inserts data before each test
     */
    public void resetDb() {

        String dropScript = ddl.generateDropDdl();
        ddl.runScript(false, dropScript);

        String createScript = ddl.generateCreateDdl();
        ddl.runScript(false, createScript);

        Map<String, List<Object>> all = (Map<String, List<Object>>) Yaml.load("initial-data.yml");
        Ebean.save(all.get("tags"));
    }

    @Test
    public void save_validTag_tagPersisted() {
        Tag tag = createTestInstance();
        assertThat(tag.id, is(nullValue()));

        tag.save();

        assertThat(tag.id, is(not(nullValue())));
    }

    @Test
    public void save_tagWithNoName_entityPersisted() {
        Tag tag = createTestInstance();
        tag.name = null;
        assertThat(tag.id, is(nullValue()));

        tag.save();

        assertThat(tag.id, is(not(nullValue())));
    }

    @Test
    public void findById_validId_tagFound() {

        Tag tag = Tag.find.byId(INIT_DATA_TEST_TAG_ID);

        assertThat(tag, is(notNullValue()));
        assertThat(tag.name, is(INIT_DATA_TEST_TAG_NAME));
    }

    @Test
    public void findById_invalidId_noTagFound() {
        Tag tag = Tag.find.byId(INIT_DATA_TEST_TAG_ID_DOESNT_EXIST);
        assertThat(tag, is(nullValue()));
    }

}
