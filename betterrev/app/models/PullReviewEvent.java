package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * PullReviewEvent that represents a lifecycle event associated with a specific PullReview.
 */
@Entity
public class PullReviewEvent extends Model {

    private static final long serialVersionUID = 188525469548289315L;

    public static Model.Finder<Long, PullReviewEvent> find = new Model.Finder<>(Long.class, PullReviewEvent.class);

    public enum PullReviewEventType {
        PULL_REVIEW_GENERATED,
        PULL_REVIEW_MODIFIED,
        MENTOR_NOTIFIED,
        APPROVED,
        MERGED,
        REJECTED,
        TERMINATED
    }

    @Id
    public Long id;

    @ManyToOne
    public PullReview pullReview;

    public PullReviewEventType pullReviewEventType;

    public String linkToExternalInfo;

    public Date createdDate;


    private PullReviewEvent() {
        this.createdDate = new Date();
    }


    public PullReviewEvent(PullReviewEventType pullReviewEventType) {
        this();
        this.pullReviewEventType = pullReviewEventType;
    }


    public PullReviewEvent(PullReviewEventType pullReviewEventType, String linkToExternalInfo) {
        this(pullReviewEventType);
        this.linkToExternalInfo = linkToExternalInfo;
    }
}
