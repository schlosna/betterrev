package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Entity that represents a concept of Interest within Betterrev.
 */
@Entity
public class Interest extends Model {

    private static final long serialVersionUID = 188525469548289315L;

    public static Finder<Long, Interest> find = new Finder<>(Long.class, Interest.class);

    @Id
    public Long id;

    public String path;

    public String project;

    public Interest(String path, String project) {
        this.path = path;
        this.project = project;
    }

    @Override
    public String toString() {
        return "Interest [id=" + id + ", path=" + path + ", project=" + project + "]";
    }

    public boolean caresAbout(String repository, String path) {
        return repository.equals(project)
            && path.matches(this.path);
    }
    
}
