package models;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Tag entity which represents a classification for a PullReview, and can be used to group PullReviews (for example
 * by Hack Day).
 */
@Entity
public class Tag extends Model {

    private static final long serialVersionUID = 188525469548289315L;

    public static Model.Finder<Long, Tag> find = new Model.Finder<>(Long.class, Tag.class);

    @Id
    public Long id;

    @Required
    public String name;

    public String description;

    public Tag(String name) {
        this.name = name;
    }


    public Tag(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
