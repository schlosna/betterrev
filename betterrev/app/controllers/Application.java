package controllers;

import java.util.List;

import models.PullReview;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result pullReviews() {
        List<PullReview> reviews = PullReview.find.all();
        return ok(pullreviews.render(reviews));
    }

    public static Result pullReview(Long id) {
        PullReview pullReview = PullReview.find.byId(id);
        return ok(pullreview.render(pullReview));
    }

    public static Result contactUs() {
        return ok(contactus.render());
    }

    public static Result help() {
        return ok(help.render());
    }

}
