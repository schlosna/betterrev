import play.Application;
import play.GlobalSettings;
import update.bitbucket.BitbucketPoller;

public class Global extends GlobalSettings {
    
    @Override
    public void onStart(Application app) {
        if (!app.isTest()) {            
            new BitbucketPoller().run();
        }
    }

}
