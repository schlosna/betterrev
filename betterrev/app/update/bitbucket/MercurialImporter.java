package update.bitbucket;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeParser;

import com.aragost.javahg.BaseRepository;
import com.aragost.javahg.Bundle;
import com.aragost.javahg.Changeset;
import com.aragost.javahg.Repository;
import com.aragost.javahg.commands.LogCommand;

import play.Logger;

/**
 * Pulls in code from a mercurial repository, keeping an internal hg repository up to date.
 */
public class MercurialImporter {
    
    private static final DateTimeFormatter format = DateTimeFormat.forPattern("> EEE MMM dd HH:mm:ss YYYY Z");
    
    private static final String SHELL = "/bin/sh";
    
    private static final String IMPORT_SCRIPT = "get_source.sh";
    
    private static final List<String> REPOS = Arrays.asList("corba", "hotspot", "jaxp", "jaxws", "jdk", "langtools", "nashorn");
    
    private final String adoptDirectory;
    private final String topLevelRepository;

    public MercurialImporter(String adoptDirectory, String topLevelRepoName) {
        this.adoptDirectory = adoptDirectory;
        topLevelRepository = adoptDirectory + "/" + topLevelRepoName;
    }
    
    public int doImport() {
        try {
            int exitCode = Processes.runProcess(topLevelRepository, SHELL, IMPORT_SCRIPT);
            
            if (exitCode != 0) {
                Logger.error("Unknown exit code when running " + IMPORT_SCRIPT + " value is " + exitCode);
            }
            
            return exitCode;
        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
            return Integer.MIN_VALUE;
        }
    }
    
    public Map<String, List<Changeset>> listChangesets(DateTime lastImport) {
        Map<String, List<Changeset>> changes = new HashMap<>();
        
        for (String repositoryName : REPOS) {
            File repositoryLocation = new File(topLevelRepository, repositoryName);
            BaseRepository repository = BaseRepository.open(repositoryLocation);
            try {
                List<Changeset> changesets = LogCommand.on(repository)
                        .date(formatDate(lastImport))
                        .execute();
                
                if (!changesets.isEmpty()) {
                    changes.put(repositoryName, changesets);
                }
            } finally {
                repository.close();
            }
        }
        
        return changes;
    }

    String formatDate(DateTime dateTime) {
        return format.print(dateTime);
    }

}
