package update.bitbucket;

import java.io.File;
import java.io.IOException;

public class Processes {
    
    public static int runProcess(String workingDirectory, String ... command) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command);
        builder.directory(new File(workingDirectory));
        
        Process process = builder.start();
        // TODO: use output from the process in order to only look at the logs of things 
        // which have been updated as a performance optimization
        // CharStreams.toString(new InputStreamReader(process.getInputStream()))
        
        try {
            return process.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
